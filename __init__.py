#! /usr/bin/env python

# <pep8 compliant>

import bpy

from . import properties
from . import operator
from . import gui

bl_info = {
    "name": "Batch Render",
    "author": "Andrea Calzavacca",
    "version": (1, 0, 0),
    "blender": (2, 91, 0),
    "location": "Render > Batch Render",
    "description": "Make possible to render more than one camera without manually start every render",
    "category": "Render"}

classes = (
    properties.CamProp,
    operator.RENDER_OT_batch,
    gui.PANEL_PT_batch_render
)

register, unregister = bpy.utils.register_classes_factory(classes)
